<?php
    $isHome = true;
    $needsLazy = true;
?>
<?php require_once 'includes/header.php'; ?>
<?php require_once 'includes/navbar.php'; ?>
    
    <header>
        <div class="header-content">
            <div class="header-content-inner">
            <h1 id="homeHeading">We build your Apps and Websites</h1>
            <hr>
            <p>Sky High Technologies is an IT company. It works towards mobile app and web application development.</p><a href="#services" class="btn btn-primary btn-xl page-scroll">Find Out More</a></div>
        </div>
    </header>
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">At Your Service</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-user text-primary sr-icons"></i>
                        <h3>User Friendly Plans</h3>
                        <p class="text-muted">Our approach is keeping the User as first priority</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <!-- <i class="fa fa-4x fa-diamond text-primary sr-icons"></i> --><i class="fa fa-android fa-4x text-primary sr-icons" aria-hidden="true"></i>
                        <h3>Android Apps</h3>
                        <p class="text-muted">Our native Android apps fulfil your requirements</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
                        <h3>Websites and Panels</h3>
                        <p class="text-muted">Everybody needs a website and a panel to manage their websites</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-graduation-cap text-primary sr-icons"></i>
                        <h3>Final Year Projects</h3>
                        <p class="text-muted">We are ready to help in final year college IT projects!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-search text-primary sr-icons"></i>
                        <h3>SEO Optimisation</h3>
                        <p class="text-muted">Empower your search engine ranking</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-share-square-o text-primary sr-icons"></i>
                        <h3>Social Media Marketing</h3>
                        <p class="text-muted">Get your work known through the power of internet</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-pencil text-primary sr-icons"></i>
                        <h3>Content Editing</h3>
                        <p class="text-muted">Keep your site and apps upto date as we provide timely maintenance</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box"><i class="fa fa-4x fa-plus text-primary sr-icons"></i>
                        <h3>and much more..</h3>
                        <p class="text-muted">We strive to provide many web and mobile services at your request.</p>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <br>
                    <br><a href="#portfolio" class="btn btn-primary btn-xl page-scroll">Checkout our work</a></div>
            </div>
        </div>
    </section>
    <section class="no-padding" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Our Portfolio</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div id="project-1-modal" class="mfp-hide white-popup-block">
            <h2>ChemTrends</h2>
            <p>Chemtrends is a chemical android mobile application.</p><img data-src="img/portfolio/Chemtrend_Collage.webp" class="img-responsive lazy" alt="Chemtrends Collage">
            <p class="text-right popup-closebutton"><a class="popup-modal-dismiss" href="#">Close</a></p>
        </div>
        <div id="project-2-modal" class="mfp-hide white-popup-block">
            <h2>Blink Hearts</h2>
            <p>Blink Hearts is a donation android mobile application.</p><img data-src="img/portfolio/Blink_Collage.webp" class="img-responsive lazy" alt="Blink Hearts Collage">
            <p class="text-right popup-closebutton"><a class="popup-modal-dismiss" href="#">Close</a></p>
        </div>
        <div id="project-3-modal" class="mfp-hide white-popup-block">
            <h2>PaynSave</h2>
            <p>PaynSave is a e-wallet android mobile application.</p><img data-src="img/portfolio/PaynSave_Collage.webp" class="img-responsive lazy" alt="PaynSave Collage">
            <p class="text-right popup-closebutton"><a class="popup-modal-dismiss" href="#">Close</a></p>
        </div>
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a class="popup-modal portfolio-box" href="#project-1-modal"><img data-src="img/portfolio/Chemtrend_Collage.webp" class="img-responsive lazy portfolio-collage-images" alt="Chemtrends Collage">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">Android Apps, Web Panels</div>
                                <div class="project-name">ChemTrends</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="popup-modal portfolio-box" href="#project-2-modal"><img data-src="img/portfolio/Blink_Collage.webp" class="img-responsive lazy portfolio-collage-images" alt="Blink Hearts Collage">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">Android Apps, Web Panels</div>
                                <div class="project-name">Blink Hearts</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="popup-modal portfolio-box" href="#project-3-modal"><img data-src="img/portfolio/PaynSave_Collage.webp" class="img-responsive lazy portfolio-collage-images" alt="PaynSave Collage">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">Android Apps</div>
                                <div class="project-name">PaynSave</div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Become one of our clients</h2><a rel="nofollow" href="#contact" class="btn btn-default btn-xl page-scroll sr-button">Contact us now!</a></div>
        </div>
    </aside>
    <section class="" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-heading">The Team</h2>
                    <hr class="primary">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/murtaza.webp" class="img img-circle lazy" alt="Murtaza Ezzy Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Murtaza Ezzy</h4><span class="text-muted founder-role">Android, Mean Stack, Server specialist</span>
                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Eat, Sleep, Code, Repeat.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/ashish.webp" class="img img-circle lazy" alt="Ashish Nair Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Ashish Nair</h4><span class="text-muted founder-role">Android, Mean Stack Developer</span>
                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Just an enthusiast</p>
                            </div>
                        </div>
                        <!-- <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<div class="founder">
								<img src="img/header.webp" class="img img-circle">
								<div class="founder-heading">
									<h4 class="founder-heading-name">Jyoti Suvarna</h4>
									<span class="text-muted founder-role">JavaScript Expert and Research Analyst</span>
									<hr class="primary founder-heading-bottomline">
								</div>
								<p class="founder-description">Some descripion about me which will interest users.</p>
							</div>
						</div> -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/tino.webp" class="img img-circle lazy" alt="Valentino Pereira Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Valentino Pereira</h4><span class="text-muted founder-role">UI, JavaScript, PHP Developer</span>
                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Living on a prayer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">About Us</h2>
                    <hr class="light">
                    <p class="text-faded">Sky High Technologies was founded on 2nd January 2018. It had started off as a just a group of freelancers, but today it is an entity which looks forward to make its mark in the IT sector.</p>
                    <p><b>SkyHigh Technologies</b>, Marol, Andheri East, Mumbai 400059.</p><a href="#contact" class="page-scroll btn btn-default btn-xl sr-button">Contact Us</a></div>
            </div>
        </div>
    </section>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center"><i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>9820036752</p>
                    <p>9769818711</p>
                    <p>9167352916</p>
                </div>
                <div class="col-lg-4 text-center"><i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:murtaza@skyhightechnologies.in">murtaza@skyhightechnologies.in</a></p>
                    <p><a href="mailto:ashish@skyhightechnologies.in">ashish@skyhightechnologies.in</a></p>
                    <p><a href="mailto:valentino@skyhightechnologies.in">valentino@skyhightechnologies.in</a></p>
                </div>
            </div>
        </div>
    </section>

<?php require_once 'includes/footer.php'; ?>