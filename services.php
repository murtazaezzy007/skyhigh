<?php require_once 'includes/header.php'; ?>
<?php require_once 'includes/navbar.php'; ?>

    <header class="header-otherpages">
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Our Services</h1>
                <hr>
            </div>
        </div>
    </header>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p>Sky High Technologies was founded on 2nd January 2018. It had started off as a just a group of freelancers, but today it is an entity which looks forward to make its mark in the IT sector.</p>
                    <p><b>SkyHigh Technologies</b>, Marol, Andheri East, Mumbai 400059.</p>
                    <hr>
                    <a href="services.php" class="btn btn-primary btn-xl">Checkout our services</a>
                </div>
            </div>
        </div>
    </section>
    
<?php require_once 'includes/footer.php'; ?>