<?php require_once 'includes/header.php'; ?>
<?php require_once 'includes/navbar.php'; ?>

    <header class="header-otherpages">
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Contact Us</h1>
                <hr>
            </div>
        </div>
    </header>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Sky High Technologies was founded on 2nd January 2018. It had started off as a just a group of freelancers, but today it is an entity which looks forward to make its mark in the IT sector.</p>
                    <p><b>SkyHigh Technologies</b>, Marol, Andheri East, Mumbai 400059.</p>
                    <hr>
                    <a href="services.php" class="btn btn-primary btn-xl">Checkout our services</a>
                </div>
                <div class="col-md-6">
                    <div id="map" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </section>

    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 19.1135724, lng: 72.876421},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4qBZZ4lMCryKTgLsDrlllkouC-z7VrP4&callback=initMap"
    async defer></script>
    
<?php require_once 'includes/footer.php'; ?>