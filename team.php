<?php $needsLazy = true; ?>
<?php require_once 'includes/header.php'; ?>
<?php require_once 'includes/navbar.php'; ?>

    <header class="header-otherpages">
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Team Members</h1>
                <hr>
            </div>
        </div>
    </header>

    <section class="" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <!-- <h2 class="section-heading">The Team</h2> -->
                    <!-- <hr class="primary"> -->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/murtaza.webp" class="img img-circle lazy" alt="Murtaza Ezzy Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Murtaza Ezzy</h4><span class="text-muted founder-role">Android, Mean Stack, Server specialist</span>
                                    <!-- <p>murtaza@skyhightechnologies.in</p> -->
                                    <!-- <p>9820098200</p> -->
                                    <!-- <div>
                                        <ul class="footer_social_icons team-social-icons" style="    ">
                                            <li><a href="https://linkedin.com/company/skyhigh-technologies" target="_blank"><i class="fa fa-linkedin"></i></a>
                                            </li>
                                            <li><a href="mailto:murtaza@skyhightechnologies.in" target="_blank"><i class="fa fa-envelope"></i></a>
                                            </li>
                                            <li><a href="https://www.facebook.com/SkyhighTechnologies" target="_blank"><i class="fa fa-facebook"></i></a>
                                            </li>
                                        </ul>
                                    </div> -->
                                    

                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Eat, Sleep, Code, Repeat.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/ashish.webp" class="img img-circle lazy" alt="Ashish Nair Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Ashish Nair</h4><span class="text-muted founder-role">Android, Mean Stack Developer</span>
                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Just an enthusiast</p>
                            </div>
                        </div>
                        <!-- <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<div class="founder">
								<img src="img/header.webp" class="img img-circle">
								<div class="founder-heading">
									<h4 class="founder-heading-name">Jyoti Suvarna</h4>
									<span class="text-muted founder-role">JavaScript Expert and Research Analyst</span>
									<hr class="primary founder-heading-bottomline">
								</div>
								<p class="founder-description">Some descripion about me which will interest users.</p>
							</div>
						</div> -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="founder"><img data-src="img/tino.webp" class="img img-circle lazy" alt="Valentino Pereira Web Developer Image">
                                <div class="founder-heading">
                                    <h4 class="founder-heading-name">Valentino Pereira</h4><span class="text-muted founder-role">UI, JavaScript, PHP Developer</span>
                                    <hr class="primary founder-heading-bottomline">
                                </div>
                                <p class="founder-description">Living on a prayer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php require_once 'includes/footer.php'; ?>