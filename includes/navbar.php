<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i></button> <a class="navbar-brand" href="index.php" rel="nofollow">SkyHigh Technologies</a></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!-- <ul class="nav navbar-nav navbar-right">
                <li><a class="page-scroll" href="#services" rel="nofollow">Services</a></li>
                <li><a class="page-scroll" href="#portfolio" rel="nofollow">Portfolio</a></li>
                <li><a class="page-scroll" href="#team" rel="nofollow">Team</a></li>
                <li><a class="page-scroll" href="#about" rel="nofollow">About</a></li>
                <li><a class="page-scroll" href="#contact" rel="nofollow">Contact</a></li>
            </ul> -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="services.php">Services</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="team.php">Team</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>