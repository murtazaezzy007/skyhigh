<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-12 footer-section">
                        <h4 class="footer-titles">SKYHIGH TECHNOLOGIES</h4>
                        <p style="color: #a8a8a8;">A professional website and app development team. We are specialists in PHP CodeIgniter, MEAN stack, Bootstrap, JavaScript web apps, BackboneJS, Socket.io</p>
                        <ul class="footer_social_icons">
                            <li>
                                <a href="https://www.facebook.com/SkyhighTechnologies" target="_blank"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://linkedin.com/company/skyhigh-technologies" target="_blank"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/skyhightech12" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 footer-section">
                        <h4 class="footer-titles">Helpful Links</h4>
                        <ul class="recent-post helpful_post">
                            <li>
                                <p><a href="services.php">Our services</a></p>
                            </li>
                            <li>
                                <p><a href="about.php">About us</a></p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 footer-section">
                        <h4 class="footer-titles">Contact Us</h4>
                        <ul class="footer_contact_info">
                            <li><i class="fa fa-phone"></i><span>Call Us - 9820036752, 9769818711</span></li>
                            <li><i class="fa fa-envelope"></i><span><a href="mailto:hello@skyhightechnologies.in">info@skyhightechnologies.in</a></span></li>
                            <li><i class="fa fa-map-marker"></i><span>Marol, Andheri East, Mumbai 400059</span></li>
                            <li><i class="fa fa-clock-o"></i><span>Monday - Saturday 11am - 7pm</span></li>
                        </ul>
                    </div>
            </div>
        </div>
        <div class="copyright">
        @2018 Skyhigh Technologies - Web development and mobile app company.
        </div>
    </footer>
    
	<script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '259726374564561',
                xfbml: true,
                version: 'v2.12'
            });
            FB.AppEvents.logPageView();
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <script src="js/main.min.js"></script>

    <?php if (isset($needsLazy)): ?>
        <script type="text/javascript" src="js/jquery.lazy.min.js"></script>

        <script type="text/javascript">
        $(function() {
            $('.lazy').Lazy();
        });
        </script>
    <?php endif; ?>

    <script>
        $(document).ready(function() {
            /* First CSS File */
            var giftofspeed = document.createElement('link');
            giftofspeed.rel = 'stylesheet';
            giftofspeed.href = 'https://fonts.googleapis.com/css?family=Open+Sans:400,500,700';
            giftofspeed.type = 'text/css';
            var godefer = document.getElementsByTagName('link')[0];
            godefer.parentNode.insertBefore(giftofspeed, godefer);
            var giftofspeed2 = document.createElement('link');
            giftofspeed2.rel = 'stylesheet';
            giftofspeed2.href = 'css/fontawesome.min.css';
            giftofspeed2.type = 'text/css';
            var godefer = document.getElementsByTagName('link')[0];
            godefer.parentNode.insertBefore(giftofspeed2, godefer);
            (function(t,e){var r=function(t){try{var r=e.head||e.getElementsByTagName("head")[0];a=e.createElement("script");a.setAttribute("type","text/javascript");a.setAttribute("src",t);r.appendChild(a);}catch(t){}};t.CollectId = "5baf3d196ebaac2ac0b9b5df";r("https://collectcdn.com/launcher.js");})(window,document);
        });
    </script>
</body>

</html>